import { Component } from '@angular/core';
import { AlertController, ToastController, ActionSheetController } from '@ionic/angular';

@Component({
  selector: 'app-home',
  templateUrl: 'home.page.html',
  styleUrls: ['home.page.scss'],
})
export class HomePage {
  tarefas: any[]=[]

  constructor(
    private AlertCtrl: AlertController,
    private toastCtrl: ToastController,
    private actionSheetCtrl: ActionSheetController
  ){
    let tarefajson = localStorage.getItem('tarefaDb');
    if(tarefajson != null){
      this.tarefas = JSON.parse(tarefajson);
    }
  }

  async addTarefa(){
    const alerta = await this.AlertCtrl.create({
      header: 'O que você precisa fazer?',
      inputs:[
        //{name: 'txtnome', type: 'text', placeholder: 'Digite Aqui...'},
        {name: 'txtpessoa', type: 'text', placeholder: 'Nome, pessoa ou juridica'},
        {name: 'txtender', type: 'text', placeholder: 'Endereço do trabalho?'},
        {name: 'txttrab', type: 'text', placeholder: 'Qual é o trabalho?'},
      ],
      buttons:[
        {
          text: 'Cancelar', role: 'cancel', cssClass: 'secondary',
          handler: ()=>{
            console.log('Você cancelou...')   // Caso o usuário clique em cancelar
          }
        },
        {
          text: 'OK', handler:(form)=>{
            //debugger;
            this.add(form);
          }
        }
      ]
    });
    alerta.present();
  }
  async add(nova: any){
    let pessoa = nova.txtpessoa;

    if(pessoa.trim().length < 1){
      const toast = await this.toastCtrl.create({
        message: "Informe o que precisa fazer",
        duration: 10000,
        position: 'middle',
        color: 'warning'
      });
      toast.present();
      return;
    }
    let tarefa = {nome: pessoa, endereco: nova.txtender, trabalho: nova.txttrab, feito: false};
    this.tarefas.push(tarefa);
    this.atualizarLocalStorage();   // Armazenar no celular local do computador
    console.log(this.tarefas);
  }
  excluir(tarefa: any){
    this.tarefas = this.tarefas.filter(a => tarefa != a);
    this.atualizarLocalStorage();
  }
  async abrirOpcoes(tarefa: any){
    const actionSheet = await this.actionSheetCtrl.create({
      header: 'escolha uma ação',
      buttons:[
        {
          text: tarefa.feito?'Desmarcar':'Marcar',
          icon: tarefa.feito?'radio-button-off':'checkmark-circle',
          handler:()=>{
            tarefa.feito = !tarefa.feito;
            this.atualizarLocalStorage();
          }
        },{
          text: 'Cancelar',
          icon: 'close',
          role: 'cancel',
          handler:()=>{
            console.log('OK, cancelado')
          }
        }
      ]
    });
    actionSheet.present();
  }
  atualizarLocalStorage(){
    localStorage.setItem('tarefaDb', JSON.stringify(this.tarefas));
  }
}
